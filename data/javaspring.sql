-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Máy chủ: db
-- Thời gian đã tạo: Th10 29, 2021 lúc 07:44 AM
-- Phiên bản máy phục vụ: 8.0.27
-- Phiên bản PHP: 7.4.20

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Cơ sở dữ liệu: `javaspring`
--

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `books`
--

CREATE TABLE `books` (
  `id` int NOT NULL,
  `category_id` int DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `price` float DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `categories`
--

CREATE TABLE `categories` (
  `id` int NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `employee`
--

CREATE TABLE `employee` (
  `id` int NOT NULL,
  `emp_name` varchar(50) NOT NULL,
  `salary` int NOT NULL,
  `job_description` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `created_at` date DEFAULT NULL,
  `updated_at` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Đang đổ dữ liệu cho bảng `employee`
--

INSERT INTO `employee` (`id`, `emp_name`, `salary`, `job_description`, `created_at`, `updated_at`) VALUES
(1, 'nguyen van a', 3000, 'ke toan', NULL, NULL),
(2, 'nguyen van b', 2000, 'xay dung', NULL, NULL),
(3, 'nguyen van c', 4000, 'phu ho', NULL, NULL);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `hibernate_sequence`
--

CREATE TABLE `hibernate_sequence` (
  `next_val` bigint DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Đang đổ dữ liệu cho bảng `hibernate_sequence`
--

INSERT INTO `hibernate_sequence` (`next_val`) VALUES
(40);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `product_tbl`
--

CREATE TABLE `product_tbl` (
  `id` int NOT NULL,
  `name` varchar(50) DEFAULT NULL,
  `price` varchar(50) NOT NULL,
  `category` varchar(50) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Đang đổ dữ liệu cho bảng `product_tbl`
--

INSERT INTO `product_tbl` (`id`, `name`, `price`, `category`, `created_at`, `updated_at`) VALUES
(1, 'note', '10000', 'sp1', NULL, NULL),
(3, 'Ha Noi', '1000.00', 'qw', NULL, NULL),
(4, 'comic', '12000', 'sp3', NULL, NULL),
(5, 'room', '13000', 'sp4', NULL, NULL),
(15, 'dd', '99', 'eee', NULL, NULL),
(16, 'd', '9', 'e', NULL, NULL),
(17, 'd', '9', 'e', NULL, NULL),
(18, 'd', '9', 'e', NULL, NULL),
(19, 'd', '9', 'e', NULL, NULL),
(20, 'drr', '9', 'e', NULL, NULL),
(21, 'drr', '9334', 'ewerwet', NULL, NULL),
(22, 'drr', '8000', 'ewerwet', NULL, NULL),
(23, 'drr2', '8000', 'ewerwet2', NULL, NULL),
(24, 'drr2', '9', 'ewerwet2', NULL, NULL),
(25, 'drr2', '90', 'ewerwet2', NULL, NULL),
(27, 'null', '9', 'ssdd', NULL, NULL),
(28, '', '9', 'ssdd', NULL, NULL),
(39, 'e', '23', 'ef', '2021-10-29 13:36:42', '2021-10-29 13:36:42');

--
-- Chỉ mục cho các bảng đã đổ
--

--
-- Chỉ mục cho bảng `books`
--
ALTER TABLE `books`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `employee`
--
ALTER TABLE `employee`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `product_tbl`
--
ALTER TABLE `product_tbl`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT cho các bảng đã đổ
--

--
-- AUTO_INCREMENT cho bảng `books`
--
ALTER TABLE `books`
  MODIFY `id` int NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
